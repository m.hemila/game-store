# Web Software Development Project Plan
Mikko Hemilä, 248925

## Description
The project is to create an online game store for JavaScript games. 
The service has two types of users: players and developers. 
Developers can add their games to the service and set a price for it. 
Players can buy games on the platform and then play purchased games online.
## Mandatory Features
#### Authentication
Project will mainly rely on Django auth and other possible Django packages. 
This is because with security it is best to rely on expert first if possible.
Also course recommends to use Django auth.

Email verification (validation in disguise) is planned for now.
#### Player Functionalities
Players will have access to atleast three screens: Shop, Games and Play.
Shop will show all available games that user can buy.
When buying a game, player will be redirected to payment process.
Games will show all the bought games.
In games screen a game can be chosen to see some statistics (scores, high score...) and start playing the game.
The play screen is meant to be just a game itself without any frills.

Possible other screens: 
- Settings (change password, for example)
#### Developer Functionalities
Developers have access to at least three screen: Inventory, Item and Analytics.
Inventory shows all the games developer has added and allows adding of new items.
Item shows data on single game and allows changing some basic things (Title, Description, Price).
Analytics shows accumulated data like how many games / sales made in total.

Possible other screens: 
- Settings (change password, for example)
#### Game/Service Interaction
Implementation details in https://plus.cs.aalto.fi/wsd/2019-2020/project/project/ (Needs access).
## Optional Features
#### RESTful API
Goal is to create RESTful endpoints and if possible, to use them overall in project (smartly).
#### Responsiveness
Project will try to create a responsive UI through using modern ways (flexbox).
Possibly hard to achieve and needs a lot of testing.
#### Extras
No extras planned / Maybe Basic Automated Testing
## Models
#### User
User django user model, should at least have
- Username / Email
- Password
#### Player(User)
- Extends user
- Hopefully no need for PII (GDPR is scary)
#### Developer(User)
- Extends user
- Company name and possibly other information like bank account number
#### Game
- Title
- Description
- URL
#### OwnedGame
- Link table between user and game (user has bought given game)
- (High) Score (possibly separate table to record all scores)
- Additional like play time?
## Getting the project done
I am a single person team with part time work and too many courses. 
This means that my time is quite limited. 
At the same time I am quite experienced as my work is a web-application.
Because of this my plan is to create the basic minimum requirements first (points an important factor).
After those I can see if I have time and intrest to implement more features.

Needless to say that cooperation will not be a problem. 
Meetings will be held face-to-face infront of mirror.
My trusted companion Rub Ducky will support development morally and by listening deeply when considering different implementation details.

Work will most likely be done on sundays as then I can concentrate on the project fully for the whole day.
## Implementation Plan
##### Week 3, 16.1. - 19.1.
- Project Plan
- Project Setup
  - Version Control in Aalto
  - Django setup
  - Very Basic Application (hello world)
- Heroku testing
##### Week 4, 20.1. - 26.1.
- Registeration
- Login and logout
##### Week 5, 27.1. - 2.2.
- List games for developer
- Playing games
- Scores
##### Week 6, 3.2.  - 9.2.
- Buying games
- Analytics
##### Week 7, 10.2  - 14.2.
- Polishing
- Testing (hahaha, good one!)
