# WSD Game Store
Mikko Hemilä, 428925

#### Authentication - 100 points
- Registering as a player or developer
- Player: Buy games, Play games
- Developer Add new games, See how many times game has been sold

#### Basic player functionalities - 200 points
- Buying games through tilkkutäkki
- Playing games
- Games shown as a crude list
- Only player (Gamer) can buy games
- Scores

#### Basic developer functionalities - 150 points
- Adding and managing game
- Can delete games, but this means it is deleted from players -> not ideal
- Show times a game has been bought
- Only developer can create games, they can manage only their own games, they can only see their own games

#### Game/service interaction - 100 points
- Player can play game
- Scores are handled

#### Quality of Work - 50 points
- DRY
- Taken advantage of Django features
- Views is somewhat messy as there is classes and functions are mixed
- No styles
- No unit/integration testing

#### Non-functional requirements 100 points
- Project plan done
- Single developer, so no teamwork
- Commits mostly named and split well
- No project management (everything done in one week and tasks kept in memory)

#### Other notes
Authentication/permissions were done in bit of a hurry. With more experience with django I think there would have been better way that to fetch permissions from database and set them for user.

As said before, I feel that mixing classes and functions in views.py makes it a little messy. It is also longer than I like and if I would continue the project I would most likely split it into multiple files.


#### Reviewing the project
Heroku: https://shrouded-ravine-51896.herokuapp.com/

1. Create a developer account
1. You should be logged in automatically
1. Create a game
1. Logout
1. Create a gamer account
1. You should be logged in
1. Go to games page
1. Buy a game
1. After payment has been finished, you should be directed to bought-games page
1. Play game
