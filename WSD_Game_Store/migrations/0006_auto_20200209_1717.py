# Generated by Django 2.2.7 on 2020-02-09 17:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('WSD_Game_Store', '0005_auto_20200209_1717'),
    ]

    operations = [
        migrations.AlterField(
            model_name='boughtgame',
            name='game',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='WSD_Game_Store.Game'),
        ),
    ]
