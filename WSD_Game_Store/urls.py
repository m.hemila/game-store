"""WSD_Game_Store URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path
from . import views
from django.views.generic.base import TemplateView
from django.views.generic import ListView
from .views import GameList, BoughtGameDetail, GameCreate, GameUpdate, GameDelete, BoughtGameList, DeveloperGameList

urlpatterns = [
    path('gamer-signup/', views.gamer_signup, name='gamer-signup'),
    path('developer-signup/', views.developer_signup, name='developer-signup'),
    path('login/', LoginView.as_view(template_name='login.html'), name="login"),
    path('logout/', LogoutView.as_view(template_name='index.html'), name="logout"),

    path('admin/', admin.site.urls),
    path('', views.home, name='home'),

    path('games/', GameList.as_view(), name='games'),
    path('games/<int:pk>/buy/', views.buy, name='game-buy'),
    path('buy/finished', views.buy_finished),

    path('bought-games/', BoughtGameList.as_view(), name='bought-games'),
    path('bought-games/<int:pk>', BoughtGameDetail.as_view(), name='game-detail'),
    path('bought-games/<int:pk>/score', views.score, name='score'),

    path('developed-games/', DeveloperGameList.as_view(), name='developed-games'),
    path('developed-games/create/', GameCreate.as_view(), name='game-create'),
    path('developed-games/<int:pk>/', GameUpdate.as_view(), name='game-update'),
    path('developed-games/<int:pk>/delete/', GameDelete.as_view(), name='game-delete'),
]
