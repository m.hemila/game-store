from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm

class Game(models.Model):
    title = models.CharField(max_length=30)
    description = models.TextField(max_length=500, blank=True)
    url = models.URLField(max_length=200)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    developer = models.ForeignKey(User, on_delete=models.CASCADE)

    def __repr__(self):
        return self.title + " " + str(self.id)

    def __str__(self):
        return self.__repr__()

class GameForm(ModelForm):
    class Meta:
        model = Game
        exclude = ['developer']

class BoughtGame(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    buyer = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('buyer', 'game')

    def __repr__(self):
        return str(self.game) + ": " + str(self.buyer)

    def __str__(self):
        return self.__repr__()

# Meant to be used for auditing
# This means that cascade should be protect instead and deletion handled differently
# But as this is school project I take the easy way.
class Payment(models.Model):
    ERROR='E'
    SUCCESS='S'
    CANCEL='C'
    STATUS = [
        (ERROR, 'Error'),
        (SUCCESS, 'Success'),
        (CANCEL, 'Cancel'),
    ]
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    buyer = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=1,choices=STATUS,null=True,default=None)
    finished = models.DateTimeField(null=True)
    reference = models.CharField(max_length=64, null=True)

class Gamer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        permissions = [('buy_game', 'Can buy games')]

class Developer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

class Score(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    score = models.IntegerField()

    class Meta:
        ordering = ['-score']
