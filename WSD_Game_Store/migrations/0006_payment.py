# Generated by Django 2.2.7 on 2020-02-10 15:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('WSD_Game_Store', '0005_auto_20200209_1752'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateField(auto_now_add=True)),
                ('status', models.CharField(choices=[('E', 'Error'), ('S', 'Success'), ('F', 'Failure')], default=None, max_length=1, null=True)),
                ('finished', models.DateField(null=True)),
                ('buyer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('game', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WSD_Game_Store.Game')),
            ],
        ),
    ]
