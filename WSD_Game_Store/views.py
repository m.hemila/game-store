from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from .models import Game, GameForm, BoughtGame, Payment, Gamer, Developer, Score
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import Permission
from hashlib import md5
from datetime import datetime
from django.http import HttpResponse
import json

seller_id = "5ZqhIU1pa2tvX1dTRA=="
payment_secret = "V_B-jadVRIAp3glwSTSRrWXX4kwA"

@login_required
def home(request):
    if hasattr(request, 'request'):
        return redirect(request.request.GET.get('next'))
    return render(request, 'index.html')

def gamer_signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        user = __create_user(form)
        if user:
            user.user_permissions.set(__gamer_permissions())
            user.save()
            Gamer.objects.create(user=user)
            login(request, user)
            return redirect('games')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})

def developer_signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        user = __create_user(form)
        if user:
            user.user_permissions.set(__developer_permissions())
            user.save()
            Developer.objects.create(user=user)
            login(request, user)
            return redirect('developed-games')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})

def __create_user(form):
    if form.is_valid():
        form.save()
        username = form.cleaned_data.get('username')
        raw_password = form.cleaned_data.get('password1')
        user = authenticate(username=username, password=raw_password)
        return user

def __developer_permissions():
    add_game = Permission.objects.get(codename='add_game')
    change_game = Permission.objects.get(codename='change_game')
    delete_game = Permission.objects.get(codename='delete_game')
    return [add_game, change_game, delete_game]

def __gamer_permissions():
    view_game = Permission.objects.get(codename='view_game')
    buy_game = Permission.objects.get(codename='buy_game')
    return [view_game, buy_game]

class GameList(LoginRequiredMixin, ListView):
    login_url = '/login/'
    model = Game

    def get_context_data(self, **kwargs):
        user = self.request.user
        context = super().get_context_data(**kwargs)
        bought_games = list(map(lambda x: x.game, BoughtGame.objects.filter(buyer=user)))
        context['bought_games'] = bought_games
        return context

class BoughtGameList(LoginRequiredMixin, ListView):
    login_url = '/login/'

    def get_queryset(self):
        return BoughtGame.objects.filter(buyer=self.request.user)

class BoughtGameDetail(LoginRequiredMixin, DetailView):
    template_name = 'WSD_Game_Store/boughtgame_detail.html'

    def get_queryset(self):
        user = self.request.user
        return BoughtGame.objects.filter(buyer=user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['scores'] = Score.objects.filter(game=self.object.game)
        return context

class DeveloperGameList(LoginRequiredMixin, ListView):
    login_url = '/login/'
    template_name = 'WSD_Game_Store/developergame_list.html'

    def get_queryset(self):
        user = self.request.user
        games = Game.objects.filter(developer=user)
        for game in games:
            times_bought = BoughtGame.objects.filter(game=game).count()
            game.times_bought = times_bought
        return games

class GameCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'WSD_Game_Store.add_game'
    login_url = '/login/'
    model = Game
    form_class = GameForm
    success_url = '/developed-games/'

    def form_valid(self, form):
        form.instance.developer = self.request.user
        return super().form_valid(form)

class GameUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'WSD_Game_Store.change_game'
    login_url = '/login/'
    model = Game
    form_class = GameForm
    success_url = '/developed-games/'

    def user_passes_test(self, request):
        if request.user.is_authenticated:
            self.object = self.get_object()
            return self.object.developer == request.user
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.user_passes_test(request):
            raise Exception("User " + str(request.user.id)+ " does not own game " + str(self.object.id))
        return super(GameUpdate, self).dispatch(request, *args, **kwargs)

class GameDelete(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    permission_required = 'WSD_Game_Store.delete_game'
    login_url = '/login/'
    model = Game
    success_url = '/developed-games/'

    def user_passes_test(self, request):
        if request.user.is_authenticated:
            self.object = self.get_object()
            return self.object.developer == request.user
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.user_passes_test(request):
            raise Exception("User " + str(request.user.id)+ " does not own game " + str(self.object.id))
        return super(GameDelete, self).dispatch(request, *args, **kwargs)

@login_required
@permission_required('WSD_Game_Store.buy_game', raise_exception=True)
def buy(request, pk):
    user = request.user
    game = get_object_or_404(Game, id=pk)
    if BoughtGame.objects.filter(buyer=user, game=game).exists():
        raise Exception("Game already bought, cannot continue")
    payment = Payment.objects.create(game=game, buyer=user)
    amount = game.price
    payment_id = str(payment.id)

    checksumstr = f"pid={payment_id:s}&sid={seller_id:s}&amount={amount:.2f}&token={payment_secret:s}"
    checksum = md5(checksumstr.encode('utf-8')).hexdigest()

    host = request.get_host()
    params = "?pid=" + payment_id
    params += "&sid=" + seller_id
    params += "&amount=" + str(amount)
    params += f"&error_url=http://{host}/buy/finished"
    params += f"&success_url=http://{host}/buy/finished"
    params += f"&cancel_url=http://{host}/buy/finished"
    params += "&checksum=" + checksum
    return redirect('https://tilkkutakki.cs.aalto.fi/payments/pay'+params)

@login_required
def buy_finished(request):
    user = request.user
    payment_id = request.GET.get('pid')
    payment = Payment.objects.get(pk=payment_id)
    reference = request.GET.get('ref')
    status_string = request.GET.get('result')
    status = __string_to_status(status_string)

    checksumstr = f"pid={payment_id:s}&ref={reference:s}&result={status_string:s}&token={payment_secret:s}"
    checksum = md5(checksumstr.encode('utf-8')).hexdigest()
    if checksum != request.GET.get('checksum'):
        __finish_payment(payment, Payment.ERROR, reference)
        raise Exception("Wrong checksum, cannot continue")
    elif user != buyer:
        __finish_payment(payment, Payment.ERROR, reference)
        raise Exception("Buyer not user, cannot continue")
    elif payment.finished == None:
        __finish_payment(payment, status, reference)
        if status == Payment.SUCCESS:
            BoughtGame.objects.get_or_create(buyer=request.user, game=payment.game)
            return redirect('bought-games')
        else:
            return redirect('games')
    else:
        raise Exception('Payment ' + str(payment.id) + ' was already compleated.')

def __string_to_status(string):
    if string == 'success':
        return Payment.SUCCESS
    elif string == 'cancel':
        return Payment.CANCEL
    elif string == 'error':
        return Payment.ERROR
    else:
        raise ValueError("Unknown status")

def __finish_payment(payment, status, reference):
    payment.status = status
    payment.finished = datetime.now()
    payment.reference = reference
    payment.save()

@login_required
def score(request, pk):
    user = request.user
    bought_game = get_object_or_404(BoughtGame, pk=pk)
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    Score.objects.create(user=user, game=bought_game.game, score=body['score'])
    return HttpResponse( content_type = "application/json")
